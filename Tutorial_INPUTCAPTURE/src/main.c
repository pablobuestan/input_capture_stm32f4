/* Includes ------------------------------------------------------------------*/
#include "main.h"

uint32_t input_capture = 0;
uint32_t count = 0;

int main(void)
{
  HAL_Init();
  SystemClock_Config();
  TIM3_Init();
  BSP_LED_Init(LED3);
  BSP_LED_Init(LED4);
  BSP_LED_Init(LED5);

  while (1)
  {
	  count = __HAL_TIM_GetCounter(&TimHandle);    //read TIM2 counter value
	  if (count == 10000)
	  {
		  BSP_LED_Toggle(LED5);
	  }
  }
}

/*##-1- Configure the TIM peripheral #######################################*/
/* TIM3 configuration: Input Capture mode ---------------------
   The external signal is connected to TIM3 CH2 pin (PB.05)
   The Rising edge is used as active edge,
   The TIM3 CCR2 is used to compute the frequency value
   Counter Clock (Hz) = 100MHz / Prescaler = 100Mhz / 10000 = 10KHz
   Counter Clock (seg) = 1 / Frec = 1 / 10KHz = 1useg
------------------------------------------------------------ */
void TIM3_Init()
{
  TimHandle.Instance = TIMx;
  TimHandle.Init.Period            = 0xFFFF;
  TimHandle.Init.Prescaler         = 10000;
  TimHandle.Init.ClockDivision     = 0;
  TimHandle.Init.CounterMode       = TIM_COUNTERMODE_UP;
  TimHandle.Init.RepetitionCounter = 0;
  HAL_TIM_IC_Init(&TimHandle);

  /* Configure the Input Capture of channel 2 */
  sICConfig.ICPolarity  = TIM_ICPOLARITY_RISING;
  sICConfig.ICSelection = TIM_ICSELECTION_DIRECTTI;
  sICConfig.ICPrescaler = TIM_ICPSC_DIV1;
  sICConfig.ICFilter    = 0;
  HAL_TIM_IC_ConfigChannel(&TimHandle, &sICConfig, TIM_CHANNEL_2);
  HAL_TIM_IC_Start_IT(&TimHandle, TIM_CHANNEL_2);
}

void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
  if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2)
  {
	input_capture= __HAL_TIM_GetCompare(&TimHandle, TIM_CHANNEL_2);    //read TIM2 channel 1 capture value
	__HAL_TIM_SetCounter(&TimHandle, 0);    //reset counter after input capture interrupt occurs
    BSP_LED_Toggle(LED4);
  }
}

// SystemCoreClock is set to 100MHz for STM32F4xx Devices.
void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;

  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();

  /* The voltage scaling allows optimizing the power consumption when the device is
     clocked below the maximum system frequency, to update the voltage scaling value
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

  /* Enable HSI Oscillator and activate PLL with HSI as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 0x10;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 400;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
}

void Error_Handler(void)
{
  /* Turn LED3 on */
  BSP_LED_On(LED3);
  while (1)
  {
  }
}
