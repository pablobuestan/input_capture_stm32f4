Using STM32F411E Discovery card with the CUBE HAL Library and the development environment "System Workbench":

This example shows how to configure the TIM peripheral in INPUT CAPTURE mode. The most appropiate way to launch this example is: 
  At the address: NAME_OF_PROJECT/inc and NAME_OF_PROJECT/src, replace the original files with those available in this repository. With these steps, the project can be compiled correctly.
  
  at the file main.c:
  
  At the beginning of the main program the HAL_Init() function is called to reset all the peripherals, initialize the Flash interface and the systick. 
  Then the SystemClock_Config() function is used to configure the system clock (SYSCLK) to run at 100 MHz.

  The function static void TIM3_Init() configure TIM3, Channel 2 as Input Capture mode where: 
  - TimHandle.Instance.- Defines the timer that you want to use (TIM3); one of its configuration channels is GPIO PB05 (channel 2).
  - TimHandle.Init.Period.- Defines the period of the account; this value has been used the maximum (65535), in this case the counter does not reset automatically when it reaches this value.
  - TimHandle.Init.Prescaler.- Reduces the frequency of the configured clock (100MHz), to obtain a counter that operates at 10KHz. 
  - HAL_TIM_IC_ConfigChannel (..., TIM_CHANNEL_2) .- Configure port PB05 as capture input.
  
  
  The HAL_TIM_IC_CaptureCallback function is associated with the interruption of a TIM in the INPUT CAPTURE method. 
  Every time a change occurs on channel 2, the value read with the function __HAL_TIM_GetCompare. 
  The value read is stored in the input_capture variable and the counter is reset using __HAL_TIM_SetCounter.